FROM node
WORKDIR /app
COPY package.json .
COPY vite.config.js .
COPY package-lock.json .
COPY cypress.config.js .
RUN npm i
COPY . .
EXPOSE 3000
CMD [ "npm","run", "dev" ]
